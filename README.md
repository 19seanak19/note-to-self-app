# Note-to-Self App

This is the application for Note-to-Self, a self-hosted notes solution. The application is currently in development. It will eventually support Linux, MacOS, Windows, Android, and iOS.

Current designs for the frontend can be found [here](https://www.figma.com/file/u9S4j1imaXxQJE2eUuwAeT/Note-to-Self-App-Designs).

The server repository can be found [here](https://gitlab.com/19seanak19/note-to-self-server).
