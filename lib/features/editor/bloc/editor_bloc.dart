import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:note_to_self/features/editor/widgets/editor_icon.dart';

part 'editor_event.dart';
part 'editor_state.dart';

class EditorBloc extends Bloc<EditorEvent, EditorState> {
  EditorBloc() : super(const EditorEditing(selectedIcons: {})) {
    on<ToolbarIconToggled>((event, emit) {
      EditorEditing oldState = state is EditorEditing
          ? state as EditorEditing
          : const EditorEditing(selectedIcons: {});

      var icon = event.icon;
      var selectedIcons = Set<EditorIconData>.from(oldState.selectedIcons);

      switch (icon) {
        case EditorIconData.bold:
        case EditorIconData.italic:
        case EditorIconData.underline:
        case EditorIconData.link:
        case EditorIconData.export:
          selectedIcons.contains(icon)
              ? selectedIcons.remove(icon)
              : selectedIcons.add(icon);
          break;
        case EditorIconData.list:
        case EditorIconData.numList:
        case EditorIconData.table:
          var alreadySelected = selectedIcons.contains(icon);
          selectedIcons.removeAll([
            EditorIconData.list,
            EditorIconData.numList,
            EditorIconData.table
          ]);
          if (!alreadySelected) selectedIcons.add(icon);
          break;
      }
      emit(EditorEditing(selectedIcons: selectedIcons));
    });
  }
}
