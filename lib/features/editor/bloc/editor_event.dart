part of 'editor_bloc.dart';

abstract class EditorEvent extends Equatable {
  const EditorEvent();

  @override
  List<Object> get props => [];
}

class ToolbarIconToggled extends EditorEvent {
  const ToolbarIconToggled(this.icon);

  final EditorIconData icon;

  @override
  List<Object> get props => [icon];
}
