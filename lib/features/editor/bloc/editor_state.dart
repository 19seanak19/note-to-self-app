part of 'editor_bloc.dart';

abstract class EditorState extends Equatable {
  const EditorState();

  @override
  List<Object> get props => [];
}

class EditorInitial extends EditorState {}

class EditorEditing extends EditorState {
  const EditorEditing({
    required this.selectedIcons,
  });

  final Set<EditorIconData> selectedIcons;

  @override
  List<Object> get props => [selectedIcons];
}
