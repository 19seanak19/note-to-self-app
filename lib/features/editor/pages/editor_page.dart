import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_to_self/features/editor/bloc/editor_bloc.dart';
import 'package:note_to_self/features/editor/widgets/editor_toolbar.dart';
import 'package:note_to_self/features/editor/widgets/editor_workspace.dart';
import 'package:note_to_self/theme/theme.dart';

class EditorPage extends StatelessWidget {
  const EditorPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EditorBloc(),
      child: Container(
        color: AppColorScheme.current.background1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const EditorToolbar(),
            Container(
              color: AppColorScheme.current.divider,
              height: 2.0,
            ),
            const Expanded(
              child: EditorWorkspace(),
            ),
          ],
        ),
      ),
    );
  }
}
