import 'package:flutter/material.dart';
import 'package:note_to_self/theme/theme.dart';

class EditorIcon extends StatefulWidget {
  static EditorIcon fromData(
    EditorIconData data, {
    Key? key,
    double size = 48.0,
    bool isSelected = false,
    Function()? onTap,
  }) {
    return EditorIcon(
      data.iconData,
      data.tipText,
      key: key,
      size: size,
      isSelected: isSelected,
      onTap: onTap,
    );
  }

  const EditorIcon(
    this.icon,
    this.tipText, {
    super.key,
    this.size = 48.0,
    this.isSelected = false,
    this.onTap,
  });

  final IconData icon;
  final String tipText;
  final double size;
  final bool isSelected;
  final Function()? onTap;

  @override
  _EditorIconState createState() => _EditorIconState();
}

class _EditorIconState extends State<EditorIcon> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => setState(() {
        isHovered = true;
      }),
      onExit: (event) => setState(() {
        isHovered = false;
      }),
      child: GestureDetector(
        onTap: widget.onTap,
        child: Tooltip(
          message: widget.tipText,
          textStyle: AppTextTheme.current.basicStyle(
            TextScale.body2,
            AppColorScheme.current.text2,
          ),
          decoration: BoxDecoration(
            color: AppColorScheme.current.background1,
            border:
                Border.all(color: AppColorScheme.current.divider, width: 2.0),
            borderRadius: const BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: Container(
            height: widget.size,
            width: widget.size,
            decoration: BoxDecoration(
              color: widget.isSelected
                  ? isHovered
                      ? AppColorScheme.current.accent.withOpacity(0.7)
                      : AppColorScheme.current.accent
                  : isHovered
                      ? AppColorScheme.current.accent.withOpacity(0.35)
                      : Colors.transparent,
              borderRadius: const BorderRadius.all(
                Radius.circular(12.0),
              ),
            ),
            child: FractionallySizedBox(
              widthFactor: 0.75,
              heightFactor: 0.75,
              child: FittedBox(
                child: Icon(
                  widget.icon,
                  color: AppColorScheme.current.text2,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

enum EditorIconData {
  bold,
  italic,
  underline,
  list,
  numList,
  table,
  link,
  export
}

extension EditorIconDataX on EditorIconData {
  IconData get iconData {
    switch (this) {
      case EditorIconData.bold:
        return Icons.format_bold;
      case EditorIconData.italic:
        return Icons.format_italic;
      case EditorIconData.underline:
        return Icons.format_underline;
      case EditorIconData.list:
        return Icons.format_list_bulleted;
      case EditorIconData.numList:
        return Icons.format_list_numbered;
      case EditorIconData.table:
        return Icons.table_chart_outlined;
      case EditorIconData.link:
        return Icons.link;
      case EditorIconData.export:
        return Icons.save;
    }
  }

  String get tipText {
    switch (this) {
      case EditorIconData.bold:
        return 'Bold';
      case EditorIconData.italic:
        return 'Italic';
      case EditorIconData.underline:
        return 'Underline';
      case EditorIconData.list:
        return 'List';
      case EditorIconData.numList:
        return 'Numbered List';
      case EditorIconData.table:
        return 'Table';
      case EditorIconData.link:
        return 'Link';
      case EditorIconData.export:
        return 'Export';
    }
  }
}
