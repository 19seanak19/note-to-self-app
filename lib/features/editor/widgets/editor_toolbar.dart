import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_to_self/features/editor/bloc/editor_bloc.dart';
import 'package:note_to_self/features/editor/widgets/editor_icon.dart';
import 'package:note_to_self/theme/theme.dart';

class EditorToolbar extends StatelessWidget {
  static const double groupPadding = 20.0;
  static const double iconPadding = 16.0;
  static const List<List<EditorIconData>> icons = [
    [
      EditorIconData.bold,
      EditorIconData.italic,
      EditorIconData.underline,
    ],
    [
      EditorIconData.list,
      EditorIconData.numList,
    ],
    [EditorIconData.table],
    [EditorIconData.link],
    [EditorIconData.export]
  ];

  const EditorToolbar({
    super.key,
    this.height = 80.0,
  });

  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColorScheme.current.background2,
      height: height,
      child: SizedBox(
        height: height,
        child: BlocBuilder<EditorBloc, EditorState>(
          builder: (context, state) {
            if (state is! EditorEditing) {
              return Container();
            }
            return ListView(
              scrollDirection: Axis.horizontal,
              children: icons
                  .map(
                    (group) => Padding(
                      padding: const EdgeInsets.only(
                        left: groupPadding + iconPadding,
                        right: groupPadding,
                      ),
                      child: Row(
                        children: group
                            .map((icon) => Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: iconPadding,
                                    ),
                                    child: EditorIcon.fromData(
                                      icon,
                                      isSelected:
                                          state.selectedIcons.contains(icon),
                                      onTap: () => context
                                          .read<EditorBloc>()
                                          .add(ToolbarIconToggled(icon)),
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                  )
                  .toList(),
            );
          },
        ),
      ),
    );
  }
}
