import 'package:flutter/material.dart';
import 'package:note_to_self/theme/theme.dart';

class EditorWorkspace extends StatelessWidget {
  static const double edgePadding = 24.0;

  const EditorWorkspace({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          color: AppColorScheme.current.editorBackground,
          child: Padding(
            padding: const EdgeInsets.only(
              left: edgePadding,
              right: edgePadding,
              top: 16.0,
              bottom: 16.0,
            ),
            child: IntrinsicHeight(
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Note Title",
                  hintStyle: AppTextTheme.current.basicStyle(
                    TextScale.heading1,
                    AppColorScheme.current.text2,
                  ),
                  filled: false,
                  border: InputBorder.none,
                ),
                style: AppTextTheme.current.basicStyle(
                  TextScale.heading1,
                  AppColorScheme.current.text1,
                ),
                cursorColor: AppColorScheme.current.accent,
                cursorWidth: 3.0,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: AppColorScheme.current.editorBackground,
            child: Padding(
              padding: const EdgeInsets.only(
                left: edgePadding,
                right: edgePadding,
                bottom: edgePadding,
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Start writing note here!",
                  hintStyle: AppTextTheme.current.basicStyle(
                    TextScale.body1,
                    AppColorScheme.current.text2,
                  ),
                  filled: false,
                  border: InputBorder.none,
                ),
                style: AppTextTheme.current.basicStyle(
                  TextScale.body1,
                  AppColorScheme.current.text1,
                ),
                cursorColor: AppColorScheme.current.accent,
                cursorWidth: 3.0,
                maxLines: null,
                expands: true,
              ),
            ),
          ),
        )
      ],
    );
  }
}
