import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'overview_event.dart';
part 'overview_state.dart';

class OverviewBloc extends Bloc<OverviewEvent, OverviewState> {
  OverviewBloc() : super(const OverviewLoaded()) {
    on<OverviewNoteTapped>((event, emit) {
      if (state is OverviewLoaded &&
          (state as OverviewLoaded).selectedNoteIndex == event.noteIndex) {
        return;
      }
      emit(OverviewLoaded(selectedNoteIndex: event.noteIndex));
    });
  }
}
