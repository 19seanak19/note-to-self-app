part of 'overview_bloc.dart';

abstract class OverviewEvent extends Equatable {
  const OverviewEvent();

  @override
  List<Object> get props => [];
}

class OverviewNoteTapped extends OverviewEvent {
  const OverviewNoteTapped({this.noteIndex = -1});

  final int noteIndex;

  @override
  List<Object> get props => [noteIndex];
}
