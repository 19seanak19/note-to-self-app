part of 'overview_bloc.dart';

abstract class OverviewState extends Equatable {
  const OverviewState();

  @override
  List<Object> get props => [];
}

class OverviewInitial extends OverviewState {}

class OverviewLoaded extends OverviewState {
  const OverviewLoaded({
    this.noteTitles = const [],
    this.noteSummaries = const [],
    this.selectedNoteIndex = -1,
  });

  final List<String> noteTitles;
  final List<String> noteSummaries;

  final int selectedNoteIndex;

  @override
  List<Object> get props => [noteTitles, noteSummaries, selectedNoteIndex];
}
