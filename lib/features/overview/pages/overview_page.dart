import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_to_self/features/overview/bloc/overview_bloc.dart';
import 'package:note_to_self/features/overview/widgets/note_card.dart';
import 'package:note_to_self/theme/theme.dart';

class OverviewPage extends StatelessWidget {
  const OverviewPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => OverviewBloc(),
      child: Container(
        width: 200.0,
        color: AppColorScheme.current.background2,
        child: Stack(
          fit: StackFit.expand,
          children: [
            BlocBuilder<OverviewBloc, OverviewState>(
              builder: (context, state) {
                if (state is! OverviewLoaded) {
                  return Container();
                }
                return ListView.separated(
                  itemCount: 12,
                  itemBuilder: (context, index) {
                    if (index == 11) {
                      return const SizedBox(height: 120.0 - 16.0);
                    }
                    return NoteCard(
                      title: "Note Title $index",
                      summary:
                          "This is the first part of the note. Only the first few lines are visible in the summary. Any more is just too much. After that the rest of the note will be cut off with an ellipses...",
                      isSelected: index == state.selectedNoteIndex,
                      onTap: () => context
                          .read<OverviewBloc>()
                          .add(OverviewNoteTapped(noteIndex: index)),
                    );
                  },
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  padding: const EdgeInsets.all(20),
                  clipBehavior: Clip.none,
                );
              },
            ),
            IgnorePointer(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 172.0,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        AppColorScheme.current.background2.withOpacity(0.0),
                        AppColorScheme.current.background2
                      ],
                      stops: const [0.0, 0.7],
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Icon(
                  Icons.settings,
                  color: AppColorScheme.current.text2,
                  size: 30.0,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  color: Colors.transparent,
                  width: 80,
                  height: 80,
                  child: FittedBox(
                    child: FloatingActionButton(
                      onPressed: null,
                      foregroundColor: AppColorScheme.current.text1,
                      backgroundColor: AppColorScheme.current.accent,
                      child: const Icon(
                        Icons.add,
                        size: 30.0,
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
