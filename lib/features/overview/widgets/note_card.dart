import 'package:flutter/material.dart';
import 'package:note_to_self/theme/theme.dart';

class NoteCard extends StatelessWidget {
  const NoteCard({
    super.key,
    this.title = "",
    this.summary = "",
    this.isSelected = false,
    this.onTap,
  });

  final String title;
  final String summary;
  final bool isSelected;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 142.0,
        padding: const EdgeInsets.all(13.0),
        decoration: BoxDecoration(
          color: AppColorScheme.current.background1,
          border: Border.all(
            color: isSelected
                ? AppColorScheme.current.accent
                : AppColorScheme.current.background1,
            width: 3.0,
            strokeAlign: StrokeAlign.inside,
          ),
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
              color: AppColorScheme.current.shadow,
              spreadRadius: 0,
              blurRadius: 10,
              offset: const Offset(0, 4),
            )
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: Text(
                title,
                style: AppTextTheme.current.basicStyle(
                  TextScale.heading2,
                  AppColorScheme.current.text1,
                ),
              ),
            ),
            Expanded(
              child: Text(
                summary,
                style: TextStyle(
                  color: AppColorScheme.current.text2,
                  fontSize: AppTextTheme.current.body2FontSize,
                  fontWeight: AppTextTheme.current.body2FontWeight,
                  fontFamily: AppTextTheme.current.fontFamily,
                ),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
    );
  }
}
