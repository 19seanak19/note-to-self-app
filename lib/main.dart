import 'package:flutter/material.dart';
import 'package:note_to_self/features/editor/editor.dart';
import 'package:note_to_self/features/overview/overview.dart';
import 'package:note_to_self/theme/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: AppColorScheme.current.accent,
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: AppColorScheme.current.accent,
          selectionColor: AppColorScheme.current.accent.withOpacity(0.7),
        ),
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            color: AppColorScheme.current.background2,
            width: 400.0,
            child: const OverviewPage(),
          ),
          Container(
            width: 2.0,
            color: AppColorScheme.current.divider,
          ),
          const Expanded(
            child: EditorPage(),
          ),
        ],
      ),
    );
  }
}
