import 'package:flutter/material.dart';

class AppColorScheme {
  static const AppColorScheme current = AppColorScheme.dark;

  static const AppColorScheme light = AppColorScheme();
  static const AppColorScheme dark = AppColorScheme(
    accent: Color.fromRGBO(68, 106, 206, 1.0),
    background1: Color.fromRGBO(73, 73, 73, 1.0),
    background2: Color.fromRGBO(36, 36, 36, 1.0),
    editorBackground: Color.fromRGBO(36, 36, 36, 1.0),
    text1: Color.fromRGBO(255, 255, 255, 1.0),
    text2: Color.fromRGBO(189, 189, 189, 1.0),
    divider: Color.fromRGBO(73, 73, 73, 1.0),
    shadow: Color.fromRGBO(0, 0, 0, 0.15),
  );

  const AppColorScheme({
    this.accent = const Color.fromRGBO(116, 167, 242, 1.0),
    this.background1 = const Color.fromRGBO(255, 255, 255, 1.0),
    this.background2 = const Color.fromRGBO(224, 224, 224, 1.0),
    this.editorBackground = const Color.fromRGBO(255, 255, 255, 1.0),
    this.text1 = const Color.fromRGBO(0, 0, 0, 1.0),
    this.text2 = const Color.fromRGBO(70, 70, 70, 1.0),
    this.divider = const Color.fromRGBO(189, 189, 189, 1.0),
    this.shadow = const Color.fromRGBO(0, 0, 0, 0.15),
  });

  final Color accent;

  final Color background1;
  final Color background2;
  final Color editorBackground;

  final Color text1;
  final Color text2;

  final Color divider;

  final Color shadow;
}
