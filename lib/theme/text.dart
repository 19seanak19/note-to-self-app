import 'package:flutter/material.dart';

enum TextScale { heading1, heading2, body1, body2 }

class AppTextTheme {
  static const AppTextTheme current = AppTextTheme();

  const AppTextTheme({
    this.fontFamily = 'Noto Sans',
    this.heading1FontSize = 24.0,
    this.heading1FontWeight = FontWeight.bold,
    this.heading2FontSize = 16.0,
    this.heading2FontWeight = FontWeight.bold,
    this.body1FontSize = 18.0,
    this.body1FontWeight = FontWeight.normal,
    this.body2FontSize = 14.0,
    this.body2FontWeight = FontWeight.normal,
  });

  final String fontFamily;

  final double heading1FontSize;
  final FontWeight heading1FontWeight;

  final double heading2FontSize;
  final FontWeight heading2FontWeight;

  final double body1FontSize;
  final FontWeight body1FontWeight;

  final double body2FontSize;
  final FontWeight body2FontWeight;

  TextStyle basicStyle(TextScale scale, Color color) {
    double fontSize;
    FontWeight fontWeight;
    switch (scale) {
      case TextScale.heading1:
        fontSize = heading1FontSize;
        fontWeight = heading1FontWeight;
        break;
      case TextScale.heading2:
        fontSize = heading2FontSize;
        fontWeight = heading2FontWeight;
        break;
      case TextScale.body1:
        fontSize = body1FontSize;
        fontWeight = body1FontWeight;
        break;
      case TextScale.body2:
        fontSize = body2FontSize;
        fontWeight = body2FontWeight;
        break;
    }
    return TextStyle(
      color: color,
      fontFamily: fontFamily,
      fontSize: fontSize,
      fontWeight: fontWeight,
    );
  }
}
